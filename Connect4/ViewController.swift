//
//  ViewController.swift
//  Connect4
//
//  Created by Steve BARRE on 23/11/2016.
//  Copyright © 2016 Seagll Games. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.        
  }
  
  @IBAction private func playButtonAction(sender: UIButton) {    
    self.presentGameGrid2()
  }
  
  private func presentGameGrid() {
    let gameGrid = GameGridViewController.instantiate()
    self.present(gameGrid, animated: true, completion: nil)
  }
  
  private func presentGameGrid2() {
    let gameGrid = GameGrid2ViewController.instantiate()
    self.present(gameGrid, animated: true, completion: nil)
  }
}

