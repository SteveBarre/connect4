//
//  GameGrid2ViewController.swift
//  Connect4
//
//  Created by Steve BARRE on 27/11/2016.
//  Copyright © 2016 Seagll Games. All rights reserved.
//

import UIKit
import Reusable

final class GameGrid2ViewController: UIViewController, StoryboardBased {

  @IBOutlet weak var verticalStackView: UIStackView!
  
  let numberOfRows = 6
  let numberOfColumns = 7
  
//  func generateGrid() {
//    
//    for _ in (0..<numberOfRows) {
////      var rowCells: [GridCellView] = []
////      for _ in (0..<numberOfColumns) {
////        let gridCell = GridCellView.loadFromNib()
////        rowCells.append(gridCell)
////      }
//      
//      var rowCells: [CircleView] = []
//      for _ in (0..<numberOfColumns) {
//        let gridCell = CircleView()
//        rowCells.append(gridCell)
//      }
//      
//      let rowHorizontalStackView = UIStackView(arrangedSubviews: rowCells)
//      rowHorizontalStackView.distribution = .fillEqually
////      rowHorizontalStackView.distribution = .equalSpacing
//      rowHorizontalStackView.axis = .horizontal
////      rowHorizontalStackView.alignment = .center
//      self.verticalStackView.addArrangedSubview(rowHorizontalStackView)
//    }
//  }
  
  func generateGrid() {
    
    for _ in (0..<numberOfRows) {
      var rowCells: [GridCellView] = []
      for _ in (0..<numberOfColumns) {
        let gridCell = GridCellView.loadFromNib()
        rowCells.append(gridCell)
      }
      
      
      let rowHorizontalStackView = UIStackView(arrangedSubviews: rowCells)
      rowHorizontalStackView.distribution = .fillEqually
      //      rowHorizontalStackView.distribution = .equalSpacing
      rowHorizontalStackView.axis = .horizontal
      //      rowHorizontalStackView.alignment = .center
      self.verticalStackView.addArrangedSubview(rowHorizontalStackView)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
//    self.verticalStackView.alignment = .center
//    self.verticalStackView.distribution = .equalSpacing
    self.generateGrid()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
//    self.verticalStackView.setNeedsLayout()
//    self.verticalStackView.layoutIfNeeded()
    
    for subview in self.verticalStackView.arrangedSubviews {
//      subview.setNeedsUpdateConstraints()
//      subview.setNeedsLayout()
//      subview.layoutIfNeeded()
//      subview.setNeedsDisplay()
      
    }
    
    let spacingRatio: CGFloat = 4/5
    
    let rowHeight = self.verticalStackView.frame.height / CGFloat(self.numberOfRows)
    let verticalSpacing = (rowHeight - (rowHeight * spacingRatio))/2
    self.verticalStackView.spacing = verticalSpacing
    
    let rowWidth = self.verticalStackView.frame.width / CGFloat(self.numberOfColumns)
    let horizontalSpacing = (rowWidth - (rowWidth * spacingRatio))/2
    
    for case let rowHorizontalStackView as UIStackView in self.verticalStackView.arrangedSubviews {
      rowHorizontalStackView.spacing = horizontalSpacing
    }
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
