//
//  GridCellView.swift
//  Connect4
//
//  Created by Steve BARRE on 27/11/2016.
//  Copyright © 2016 Seagll Games. All rights reserved.
//

import UIKit
import Reusable

final class LayerBehavior: NSObject {
  @IBInspectable var cornerRadius: CGFloat = 5
  
  @IBOutlet var view: UIView! {
    didSet {
      self.view.layer.cornerRadius = self.cornerRadius
    }
  }
}

final class GridCellView: UIView, NibOwnerLoadable {

  @IBOutlet private weak var circleView: UIView!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    GridCellView.loadFromNib(owner: self)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.layoutIfNeeded()
    
//    self.layer.borderColor = UIColor.blue.cgColor
//    self.layer.borderWidth = 1.0
    
    self.circleView.backgroundColor = UIColor.white
    
//    self.circleView.layer.borderColor = UIColor.black.cgColor
//    self.circleView.layer.borderWidth = 1.0
    
//    let circleRadius = min(self.circleView.frame.size.width, self.circleView.frame.size.height)
//    
    self.circleView.layer.cornerRadius = self.circleView.frame.width / 2
    self.circleView.layer.masksToBounds = true
  }
}
