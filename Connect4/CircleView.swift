//
//   Draw CircleView.swift
//  Connect4
//
//  Created by Steve BARRE on 28/11/2016.
//  Copyright © 2016 Seagll Games. All rights reserved.
//

import UIKit

class CircleView: UIView {
  
  var radius: CGFloat {
    return min(bounds.size.width, bounds.size.height) / 2
  }
  
  var centerCoordinate: CGPoint {
    return CGPoint(x: bounds.size.width/2, y: bounds.size.height/2)
  }
  
  override init (frame : CGRect) {
    super.init(frame : frame)
//    self.backgroundColor = UIColor.red
//    self.translatesAutoresizingMaskIntoConstraints = true
    
  }
  
  convenience init () {
    self.init(frame:CGRect.zero)
  }
  
  required init(coder aDecoder: NSCoder) {
    fatalError("This class does not support NSCoding")
  }
  
//  override func draw(_ rect: CGRect) {
//    // Get the Graphics Context
//    let context = UIGraphicsGetCurrentContext()
//    context?.addEllipse(in: rect)
//    context?.setFillColor(UIColor.white.cgColor)
//    context?.fillPath()
//  }
  
  override func draw(_ rect: CGRect) {
    // Get the Graphics Context
    
    let rad = min(rect.width, rect.height)
    
//    let rad = radius*2
    
      let nrect = CGRect(x: rect.origin.x, y: rect.origin.y, width: rad, height: rad)
    
    let context = UIGraphicsGetCurrentContext()
    context?.addEllipse(in: nrect)
    context?.setFillColor(UIColor.white.cgColor)
    context?.fillPath()
  }
  
//      override func draw(_ rect: CGRect) {
//        // Get the Graphics Context
//    
//        let circlePath = UIBezierPath(arcCenter: self.centerCoordinate,
//                                      radius: CGFloat(radius),
//                                      startAngle: CGFloat(0),
//                                      endAngle:CGFloat(M_PI * 2),
//                                      clockwise: true)
//  
//        circlePath.stroke()
//      }
  
//    override func draw(_ rect: CGRect) {
//      // Get the Graphics Context
////      var context = UIGraphicsGetCurrentContext()
//      
//      
//      
//      let circlePath = UIBezierPath(arcCenter: self.centerCoordinate,
//                                    radius: CGFloat(radius),
//                                    startAngle: CGFloat(0),
//                                    endAngle:CGFloat(M_PI * 2),
//                                    clockwise: true)
//      
////      context?.addArc(center: centerPoint, radius: self.radius, startAngle: 0, endAngle: CGFloat(M_PI * 2), clockwise: true)
////      context?.setFillColor(UIColor.white.cgColor)
//      
//      // Draw
////      context?.strokePath()
//      circlePath.stroke()
//    }

}
