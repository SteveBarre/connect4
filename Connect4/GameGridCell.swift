//
//  GameGridCell.swift
//  Connect4
//
//  Created by Steve BARRE on 23/11/2016.
//  Copyright © 2016 Seagll Games. All rights reserved.
//

import UIKit
import Reusable

final class GameGridCell: UICollectionViewCell, NibReusable {

  @IBOutlet weak var circleView: UIView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.circleView.layer.cornerRadius = self.circleView.frame.size.width / 2
    self.circleView.layer.masksToBounds = true
  }

}
