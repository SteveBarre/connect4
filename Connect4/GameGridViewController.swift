//
//  GameGridViewController.swift
//  Connect4
//
//  Created by Steve BARRE on 23/11/2016.
//  Copyright © 2016 Seagll Games. All rights reserved.
//

import UIKit
import Reusable

final class GameGridViewController: UIViewController, StoryboardBased {

  @IBOutlet weak var collectionView: UICollectionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.setupCollectionView()
  }
  
  private func setupCollectionView() {
    self.collectionView.register(cellType: GameGridCell.self)
    if let flowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      flowLayout.itemSize = CGSize.init(width: 50, height: 50)
    }
    
    self.collectionView.reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

extension GameGridViewController: UICollectionViewDelegate {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 6
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 7
  }
}

extension GameGridViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let gridCell = self.collectionView.dequeueReusableCell(for: indexPath) as GameGridCell
    return gridCell
  }
}
